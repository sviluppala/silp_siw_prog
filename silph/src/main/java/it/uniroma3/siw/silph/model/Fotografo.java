package it.uniroma3.siw.silph.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "fotografi")
public class Fotografo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "bioPic")
	private String bioPic;
	
	@Column(name = "album")
	@OneToMany(mappedBy = "autore" , cascade = { CascadeType.REMOVE } )
	private List<Album> album;
	
	public Fotografo() {
		//no_Args
	}
	
	public Fotografo(String nome, String cognome, String bioPic) {
		super();
		this.firstName = nome;
		this.lastName = cognome;
		this.bioPic = bioPic;
	}
	
	protected String getNome() {
		return firstName;
	}
	
	protected void setNome(String nome) {
		this.firstName = nome;
	}
	
	protected String getCognome() {
		return lastName;
	}
	
	protected void setCognome(String cognome) {
		this.lastName = cognome;
	}
	
	protected String getBioPic() {
		return bioPic;
	}
	
	protected void setBioPic(String bioPic) {
		this.bioPic = bioPic;
	}
	
	protected List<Album> getAlbum() {
		return album;
	}
	
	protected void setAlbum(List<Album> album) {
		this.album = album;
	}
	
	protected void setId(Long id){
		this.id = id;
	}
	
	protected Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Album album = (Album)obj;
		return album.getId()==this.getId();
	}
	
	
	
}
