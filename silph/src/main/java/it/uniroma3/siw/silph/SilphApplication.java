package it.uniroma3.siw.silph;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import it.uniroma3.siw.silph.model.Album;
import it.uniroma3.siw.silph.model.Foto;
import it.uniroma3.siw.silph.model.Fotografo;
import it.uniroma3.siw.silph.service.FotoService;

@SpringBootApplication
@ComponentScan(basePackages = { "it.*"})
public class SilphApplication {
	
	@Autowired
	private FotoService repo;

	public static void main(String[] args) {
		SpringApplication.run(SilphApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		String desc="ha spaziato con i suoi reportage in più generi, "
				+ "dalla street photography alla fotografia di guerra "
				+ "e dalla fotografia urbana al ritratto";
		Fotografo primoF = new Fotografo("Steve","McCurry", desc);
		Album a = new Album("aa",primoF);
		Foto f = new Foto(primoF,a);
		repo.inserisci(f);
		
	}

}
