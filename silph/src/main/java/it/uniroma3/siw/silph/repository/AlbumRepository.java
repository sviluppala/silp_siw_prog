package it.uniroma3.siw.silph.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.silph.model.Album;

@Repository
public interface AlbumRepository extends CrudRepository<Album, Long>{
	
	public List<Album> findByTitolo(String titolo);
	public List<Album> findByAutore(String autore);

}
