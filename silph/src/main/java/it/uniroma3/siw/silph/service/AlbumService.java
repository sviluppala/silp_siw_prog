package it.uniroma3.siw.silph.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.model.Album;
import it.uniroma3.siw.silph.repository.AlbumRepository;


@Transactional
@Service
public class AlbumService {
	
	@Autowired
	private AlbumRepository repo;
	
	public Album inserisci(Album album){
		return repo.save(album);
	}
	
	public void delete(Album album) {
		repo.delete(album);
	}
	
	public List<Album> findAll(){
		return (List<Album>)repo.findAll();
	}
	
	public List<Album> findByTitolo(String titolo){
		return repo.findByTitolo(titolo);
	}

	public List<Album> findByAutore(String autore){
		return repo.findByAutore(autore);
	}
}
