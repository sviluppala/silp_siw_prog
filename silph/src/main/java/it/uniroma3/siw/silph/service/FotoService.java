package it.uniroma3.siw.silph.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.model.Foto;
import it.uniroma3.siw.silph.repository.FotoRepository;


@Service
@Transactional
public class FotoService {
	
	@Autowired
	private FotoRepository repo;
	
	public Foto inserisci(Foto foto){
		return this.repo.save(foto);
	}
	
	public void delete(Foto foto){
		this.repo.delete(foto);
	}
	
	public List<Foto> findAll(){
		return (List<Foto>) this.repo.findAll();
	}
	
	public List<Foto> findByTag(String tag){
		return this.repo.findByTag(tag);
	}

}

