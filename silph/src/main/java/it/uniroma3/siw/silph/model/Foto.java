package it.uniroma3.siw.silph.model;

import java.awt.Image;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "foto")
public class Foto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id" , nullable = false)
	private Long id;
	
	@Column(name = "tag")
	private List<String> tag;
	
	@Column(name = "autore")
	@ManyToOne
	private Fotografo Autore;
	
	@Column(name = "album")
	@ManyToOne
	private Album album;
	
	@Column(name = "foto")
	private Image foto;
	
	public Foto() {
		//no_Args constructor is required by JPA standard
	}
	
	public Foto(Fotografo autore, Album album){
		super();
		this.Autore = autore;
		this.album = album;
	}
	
	protected Long getId() {
		return id;
	}
	
	protected void setId(Long id) {
		this.id = id;
	}
	
	protected List<String> getTag() {
		return tag;
	}
	
	protected void setTag(List<String> tag) {
		this.tag = tag;
	}
	
	protected Fotografo getAutore() {
		return Autore;
	}
	
	protected void setAutore(Fotografo autore) {
		Autore = autore;
	}
	
	protected Album getAlbum() {
		return album;
	}
	
	protected void setAlbum(Album album) {
		this.album = album;
	}
	
	protected Image getFoto() {
		return foto;
	}
	
	protected void setFoto(Image foto) {
		this.foto = foto;
	}
	
}
