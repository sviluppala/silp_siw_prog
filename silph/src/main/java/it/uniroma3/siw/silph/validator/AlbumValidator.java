package it.uniroma3.siw.silph.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.silph.model.Album;

@Component
public class AlbumValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> arg0) {
		return Album.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nome", "necessario");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "autore", "necessario");
	}

}
