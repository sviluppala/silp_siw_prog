package it.uniroma3.siw.silph.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.silph.model.Fotografo;

@Repository
public interface FotografoRepository extends CrudRepository<Fotografo, Long> {

	public List<Fotografo> findByName(String name);
	public List<Fotografo> findBySurname(String cognome);
	public List<Fotografo> findByBoth(String nome, String cognome);

}
