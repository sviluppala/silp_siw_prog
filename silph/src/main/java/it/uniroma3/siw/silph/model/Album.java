package it.uniroma3.siw.silph.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "albums")
public class Album {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "titolo")
	private String titolo;
	
	@Column(name = "album")
	@OneToMany(mappedBy = "album" , cascade = { CascadeType.REMOVE } )
	private List<Foto> album;
	
	@Column(name = "autore")
	@ManyToOne
	private Fotografo autore;
	
	public Album() {
		//no_Args
	}
	
	public Album(String nome, Fotografo autore){
		super();
		this.titolo = nome;
		this.autore = autore;
	}
	
	protected Long getId() {
		return id;
	}
	
	protected void setId(Long id) {
		this.id = id;
	}
	
	protected String getNome() {
		return titolo;
	}
	
	protected void setNome(String nome) {
		this.titolo = nome;
	}
	
	protected List<Foto> getAlbum() {
		return album;
	}
	
	protected void setAlbum(List<Foto> album) {
		this.album = album;
	}
	
	protected Fotografo getAutore() {
		return autore;
	}
	
	protected void setAutore(Fotografo autore) {
		this.autore = autore;
	}

}
