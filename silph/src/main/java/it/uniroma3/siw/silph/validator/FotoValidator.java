package it.uniroma3.siw.silph.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.silph.model.Foto;

@Component
public class FotoValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> arg0) {
		return Foto.class.equals(arg0);
	}
	
	@Override
	public void validate(Object arg0, Errors arg1) {
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "album", "necessario");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "autore", "necessario");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "foto", "necessario");
	}

}
