package it.uniroma3.siw.silph.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.silph.model.Foto;

@Repository
public interface FotoRepository extends CrudRepository<Foto, Long> {
	
	public List<Foto> findByTag(String tag);
	
}
