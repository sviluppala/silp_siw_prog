package it.uniroma3.siw.silph.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.model.Fotografo;
import it.uniroma3.siw.silph.repository.FotografoRepository;


@Transactional
@Service
public class FotografoService {
	
	@Autowired
	private FotografoRepository repo;
	
	
	public Fotografo inserisci(Fotografo fotografo){
		return repo.save(fotografo);
	}
	
	public void delete(Fotografo fotografo) {
		repo.delete(fotografo);
	}
	
	public List<Fotografo> tutti(){
		return (List<Fotografo>) repo.findAll();
	}
	
	public List<Fotografo> findByNAme(String name) {
		return repo.findByName(name);
	}
	
	public List<Fotografo> findBySurname(String cognome){
		return repo.findBySurname(cognome);
	}
	
	public List<Fotografo> findByBoth(String nome, String cognome){
		return repo.findByBoth(nome, cognome);
	}

}